//Method-local Inner Classes
/*Un m�todo de clase interna local se puede instanciar s�lo 
 dentro del m�todo donde se define la clase interna.*/
public class Clase_metodolocal {
	   // instancia del metodo de la clase externa
	   void metodo() {
	      int num = 10;

	      // method-local inner class
	      class MethodInner_Demo {
	         public void print() {
	            System.out.println("This is method inner class "+num);	   
	         }   
	      } // final de la inner class
		   
	      // Accesos a la inner class
	      MethodInner_Demo inner = new MethodInner_Demo();
	      inner.print();
	   }
	   
	   public static void main(String args[]) {
		   Clase_metodolocal outer = new Clase_metodolocal();
	      outer.metodo();	   	   
	   }
}
