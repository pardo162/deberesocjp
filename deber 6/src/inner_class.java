// Inner classes
/*Para crear una inner class tienes que escribir una 
  clase dentro de una clase. 
  A diferencia de una clase, 
  una clase interna puede ser privada y 
  una vez que declara una clase interna privada, 
  no se puede acceder desde un objeto fuera
   de la clase.*/
public class inner_class {
	 int num;
	   
	   // inner class
	   private class Inner_Demo {
	      public void print() {
	         System.out.println("Esto es una inner class");
	      }
	   }
	   
	   //El acceso a la clase interna
	   void display_Inner() {
	      Inner_Demo inner = new Inner_Demo();
	      inner.print();
	   }
}

